# Pythono3 code to rename multiple 
# files in a directory or folder 

# importing os module 
import os 

# Function to rename multiple files 
def main(): 
	i = 0
	
	for filename in os.listdir("C:/Users/amroy/Desktop/transparency/lab_in_the_field/test_tenders/"): 
		dst =str(i) + ".png"
		src ='C:/Users/amroy/Desktop/transparency/lab_in_the_field/test_tenders/'+ filename 
		dst ='C:/Users/amroy/Desktop/transparency/lab_in_the_field/test_tenders/'+ dst 
		
		# rename() function will 
		# rename all the files 
		os.rename(src, dst) 
		i += 1

# Driver Code 
if __name__ == '__main__': 
	
	# Calling main() function 
	main() 
