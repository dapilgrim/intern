############################################################
#           TENDER                                         #
#        GENERATION                                        #
#          PROGRAM                                         #
#                                                          #
############################################################ 
#Project: Transparency Uganda 
#Author: Asiku Roy Alia


#Imports for the conversion from pdf to png 
import random
import os 
from pdf2image import convert_from_path 
from pdf2image.exceptions import (
	PDFInfoNotInstalledError, 
	PDFPageCountError, 
	PDFSyntaxError) 


#Imports for the conversion from docx to pdf 

import sys
import comtypes.client


# Imports for the mailmerge operation
from mailmerge import MailMerge
import openpyxl
import pandas as pd 

#Other imports 
import datetime

#Define the template lists 
case_16 = ["C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_16_1.docx", "C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_16_2.docx"]
case_15 = ["C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_15_1.docx", "C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_15_2.docx"]
case_14 = ["C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_14_1.docx", "C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_14_2.docx"]
case_13 = ["C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_13_1.docx", "C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_13_2.docx"]
case_12 = ["C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_12_1.docx", "C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_12_2.docx"]
case_11 = ["C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_11_1.docx", "C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_11_2.docx"]
case_10 = ["C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_10_1.docx", "C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_10_2.docx"]
case_9 = ["C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_9_1.docx", "C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_9_2.docx"]
case_8 = ["C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_8_1.docx", "C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_8_2.docx"]
case_7 = ["C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_7_1.docx", "C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_7_2.docx"]
case_6 = ["C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_6_1.docx", "C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_6_2.docx"]
case_5 = ["C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_5_1.docx", "C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_5_2.docx"]
case_4 = ["C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_4_1.docx", "C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_4_2.docx"]
case_3 = ["C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_3_1.docx", "C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_3_2.docx"]
case_2 = ["C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_2_1.docx", "C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_2_2.docx"]
case_1 = ["C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_1_1.docx", "C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_1_2.docx"]

#Load the source data and choose the sheet to work with 
workbook = openpyxl.load_workbook('C:/Users/amroy/Dropbox/Newsletter/lab-in-the-field/Tender_Generator_Python/Output/output_mail_merge_test.xlsx')
sheet = workbook["Sheet1"]
max_col = sheet.max_row
sheet.delete_rows(sheet.min_row,1)


#Randomly choose the templates to use 
for i in range(1, max_col):
	if (str(sheet.cell(row=i, column=18).value)=="1") and \
	   (str(sheet.cell(row=i, column=19).value)=="1") and \
	   (str(sheet.cell(row=i, column=17).value)=="1") and \
	   (str(sheet.cell(row=i, column=20).value)=="1"):
		template=case_16[(i*random.randint(4,50))%2]
		
	elif (str(sheet.cell(row=i, column=18).value)=="1") and \
	     (str(sheet.cell(row=i, column=19).value)=="1") and \
	     (str(sheet.cell(row=i, column=17).value)=="1") and \
	     (str(sheet.cell(row=i, column=20).value)=="0"):
		
		template=case_15[(i*random.randint(4,60))%2]
	elif (str(sheet.cell(row=i, column=18).value)=="1") and \
	     (str(sheet.cell(row=i, column=19).value)=="1") and \
	     (str(sheet.cell(row=i, column=17).value)=="0") and \
	     (str(sheet.cell(row=i, column=20).value)=="1"):
		template=case_14[(i*random.randint(4,70))%2]
		#template=str(random.choices(case_14)).replace('[','').replace(']','')
	elif (str(sheet.cell(row=i, column=18).value)=="1") and \
	     (str(sheet.cell(row=i, column=19).value)=="1") and \
	     (str(sheet.cell(row=i, column=17).value)=="0") and \
	     (str(sheet.cell(row=i, column=20).value)=="0"):
		
		template=case_13[(i*random.randint(4,80))%2]
	elif (str(sheet.cell(row=i, column=18).value)=="1") and \
	     (str(sheet.cell(row=i, column=19).value)=="0") and \
	     (str(sheet.cell(row=i, column=17).value)=="1") and \
	     (str(sheet.cell(row=i, column=20).value)=="1"):
		
		template=case_12[(i*random.randint(4,90))%2]
	elif (str(sheet.cell(row=i, column=18).value)=="1") and \
	     (str(sheet.cell(row=i, column=19).value)=="0") and \
	     (str(sheet.cell(row=i, column=17).value)=="1") and \
	     (str(sheet.cell(row=i, column=20).value)=="0"):
		
		template=case_11[(i*random.randint(4,40))%2] 
	elif (str(sheet.cell(row=i, column=18).value)=="1") and \
	     (str(sheet.cell(row=i, column=19).value)=="0") and \
	     (str(sheet.cell(row=i, column=17).value)=="0") and \
	     (str(sheet.cell(row=i, column=20).value)=="1"):
		
		template=case_10[(i*random.randint(4,30))%2]
	elif (str(sheet.cell(row=i, column=18).value)=="1") and \
	     (str(sheet.cell(row=i, column=19).value)=="0") and \
	     (str(sheet.cell(row=i, column=17).value)=="0") and \
	     (str(sheet.cell(row=i, column=20).value)=="0"):
		
		template=case_9[(i*random.randint(4,20))%2]
	elif (str(sheet.cell(row=i, column=18).value)=="0") and \
	     (str(sheet.cell(row=i, column=19).value)=="1") and \
	     (str(sheet.cell(row=i, column=17).value)=="1") and \
	     (str(sheet.cell(row=i, column=20).value)=="1"):
		
		template=case_8[(i*random.randint(4,10))%2]
	elif (str(sheet.cell(row=i, column=18))=="0") and \
	     (str(sheet.cell(row=i, column=19).value)=="1") and \
	     (str(sheet.cell(row=i, column=17).value)=="1") and \
	     (str(sheet.cell(row=i, column=20).value)=="0"):
		
		template=case_7[(i*random.randint(4,55))%2]
	elif (str(sheet.cell(row=i, column=18).value)=="0") and \
	     (str(sheet.cell(row=i, column=19).value)=="1") and \
	     (str(sheet.cell(row=i, column=17).value)=="0") and \
	     (str(sheet.cell(row=i, column=20).value)=="1"):
		
		template=case_6[(i*random.randint(4,57))%2]
	elif (str(sheet.cell(row=i, column=18).value)=="0") and \
	     (str(sheet.cell(row=i, column=19).value)=="1") and \
	     (str(sheet.cell(row=i, column=17).value)=="0") and \
	     (str(sheet.cell(row=i, column=20).value)=="0"):
		
		template=case_5[(i*random.randint(4,56))%2]
	elif (str(sheet.cell(row=i, column=18).value)=="0") and \
	     (str(sheet.cell(row=i, column=19).value)=="0") and \
	     (str(sheet.cell(row=i, column=17).value)=="1") and \
	     (str(sheet.cell(row=i, column=3).value)=="1"):
		
		template=case_4[(i*random.randint(4,58))%2]
	elif (str(sheet.cell(row=i, column=18).value)=="0") and \
	     (str(sheet.cell(row=i, column=19).value)=="0") and \
	     (str(sheet.cell(row=i, column=17).value)=="1") and \
	     (str(sheet.cell(row=i, column=20).value)=="0"):
		
		template=case_3[(i*random.randint(4,59))%2]
	elif (str(sheet.cell(row=i, column=18).value)=="0") and \
	     (str(sheet.cell(row=i, column=19).value)=="0") and \
	     (str(sheet.cell(row=i, column=17).value)=="0") and \
	     (str(sheet.cell(row=i, column=20).value)=="1"):
		
		template=case_2[(i*random.randint(4,54))%2]
	

	else:
		#Last option code
		template=case_1[(i*random.randint(4,54))%2]
	print(template)
	print("\n")

    #Implement the mailmerge operation
	document1=MailMerge(template)
	document1.merge(Entity_Name=str(sheet.cell(row=i, column=1).value),	bidding_document_price=str(sheet.cell(row=i, column=2).value),bidding_document_security=str(sheet.cell(row=i, column=3).value),	Procurement_Method=str(sheet.cell(row=i, column=4).value), Location=str(sheet.cell(row=i, column=5).value),	tender_subject=str(sheet.cell(row=i, column=6).value),	publish_date=str(sheet.cell(row=i, column=7).value)[:-8],Prebid_Meeting_Date=str(sheet.cell(row=i, column=8).value)[:-8],	Bid_Open_Date=str(sheet.cell(row=i, column=9).value)[:-8],	Bid_Closing=str(sheet.cell(row=i, column=10).value)[:-8], Bid_Winner_Declaration=str(sheet.cell(row=i, column=11).value)[:-8],	Contract_Signing=str(sheet.cell(row=i, column=12).value)[:-8], Address=str(sheet.cell(row=i, column=13).value), 	Telephone_Number=str(sheet.cell(row=i, column=14).value))
	document1.write(str(sheet.cell(row=i, column=21).value)+".docx")
	created_file=str(sheet.cell(row=i, column=21).value)+".docx"
	print("File"+created_file+"created!!!\n")




#Code for the conversion from docx to pdf 
wdFormatPDF = 17

base_path="C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/"
os.chdir(base_path)
with os.scandir(base_path) as entries:
	for entry in entries:
		if entry.is_file() and (entry.name[-4:]=="docx"):
			in_file=base_path+entry.name
			out_file=base_path+entry.name[:-4]+"pdf"
			word = comtypes.client.CreateObject('Word.Application')
			doc = word.Documents.Open(in_file)
			doc.SaveAs(out_file, FileFormat=wdFormatPDF)
			doc.Close()
			print("Document"+out_file+"created!!!")
			word.Quit()  


#Code for the conversion from pdf to png 

basepath="C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/"

#os.chdir("basepath")
with os.scandir(basepath) as entries:
	for entry in entries:
		if entry.is_file():
			if entry.name[-3:]=="pdf":
				input_filename=basepath+"/"+entry.name
				images=convert_from_path(input_filename)
				for i, image in enumerate(images):
					fname=entry.name[:-4]+".PNG"
					image.save(fname,"PNG")
					print("Document"+fname+"created!!!\n")


"""
#The commented code includes other versions of this program with less features

#This program converts a pdf document into multiple images 
from pdf2image import convert_from_path 
from pdf2image.exceptions import (
	PDFInfoNotInstalledError, 
	PDFPageCountError, 
	PDFSyntaxError) 
images=convert_from_path('C:/Users/amroy/Dropbox/Newsletter/user_activity/Programs/asiku_programs/output.pdf')
for i, image in enumerate(images):
	fname="image" + str(i) + ".png"
	image.save(fname,"PNG") 

import os
#This program converts a pdf document into multiple images 
from pdf2image import convert_from_path 
from pdf2image.exceptions import (
	PDFInfoNotInstalledError, 
	PDFPageCountError, 
	PDFSyntaxError) 


basepath="C:/Users/amroy/Desktop/transparency/lab_in_the_field/pdf"

#os.chdir("basepath")
with os.scandir(basepath) as entries:
	for entry in entries:
		if entry.is_file():
			input_filename=basepath+"/"+entry.name
			images=convert_from_path(input_filename)
			for i, image in enumerate(images):
				fname=entry.name[:-4]+".PNG"
				image.save(fname,"PNG")

"""
#***********************************
#   Start program execution        *
#                                  *
#                                  *
#***********************************
'''
#Imports for the conversion from pdf to png 
import os 
from pdf2image import convert_from_path 
from pdf2image.exceptions import (
	PDFInfoNotInstalledError, 
	PDFPageCountError, 
	PDFSyntaxError) 

#Imports for the conversion from docx to pdf 

import sys
import comtypes.client




# Imports for the mailmerge operation
from mailmerge import MailMerge
import openpyxl
import pandas as pd

#Code for mail merge 
workbook=openpyxl.load_workbook('C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/output_mail_merge.xlsx')
sheet=workbook["Sheet1"]
max_col=sheet.max_row
sheet.delete_rows(sheet.min_row,1)
for i in range(1, max_col):
	template="C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_006.docx"
	template1="C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_017.docx"
	template2="C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_019.docx"
	template3="C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_023.docx"
	template4="C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/Template_027.docx"
	document1=MailMerge(template)
	document1.merge(Entity_Name=str(sheet.cell(row=i, column=1).value),	bidding_document_price=str(sheet.cell(row=i, column=2).value),bidding_document_security=str(sheet.cell(row=i, column=3).value),	Procurement_Method=str(sheet.cell(row=i, column=4).value), Location=str(sheet.cell(row=i, column=5).value),	tender_subject=str(sheet.cell(row=i, column=6).value),	publish_date=str(sheet.cell(row=i, column=7).value),Prebid_Meeting_Date=str(sheet.cell(row=i, column=8).value),	Bid_Open_Date=str(sheet.cell(row=i, column=9).value),	Bid_Closing=str(sheet.cell(row=i, column=10).value), Bid_Winner_Declaration=str(sheet.cell(row=i, column=11).value),	Contract_Signing=str(sheet.cell(row=i, column=12).value), Address=str(sheet.cell(row=i, column=13).value), 	Telephone_Number=str(sheet.cell(row=i, column=14).value))
	#os.mkdir("word_docs")
	document2=MailMerge(template1)
	document2.merge(Entity_Name=str(sheet.cell(row=i, column=1).value),	bidding_document_price=str(sheet.cell(row=i, column=2).value),bidding_document_security=str(sheet.cell(row=i, column=3).value),	Procurement_Method=str(sheet.cell(row=i, column=4).value), Location=str(sheet.cell(row=i, column=5).value),	tender_subject=str(sheet.cell(row=i, column=6).value),	publish_date=str(sheet.cell(row=i, column=7).value),Prebid_Meeting_Date=str(sheet.cell(row=i, column=8).value),	Bid_Open_Date=str(sheet.cell(row=i, column=9).value),	Bid_Closing=str(sheet.cell(row=i, column=10).value), Bid_Winner_Declaration=str(sheet.cell(row=i, column=11).value),	Contract_Signing=str(sheet.cell(row=i, column=12).value), Address=str(sheet.cell(row=i, column=13).value), 	Telephone_Number=str(sheet.cell(row=i, column=14).value))
	#os.mkdir("word_docs")
	document3=MailMerge(template2)
	document3.merge(Entity_Name=str(sheet.cell(row=i, column=1).value),	bidding_document_price=str(sheet.cell(row=i, column=2).value),bidding_document_security=str(sheet.cell(row=i, column=3).value),	Procurement_Method=str(sheet.cell(row=i, column=4).value), Location=str(sheet.cell(row=i, column=5).value),	tender_subject=str(sheet.cell(row=i, column=6).value),	publish_date=str(sheet.cell(row=i, column=7).value),Prebid_Meeting_Date=str(sheet.cell(row=i, column=8).value),	Bid_Open_Date=str(sheet.cell(row=i, column=9).value),	Bid_Closing=str(sheet.cell(row=i, column=10).value), Bid_Winner_Declaration=str(sheet.cell(row=i, column=11).value),	Contract_Signing=str(sheet.cell(row=i, column=12).value), Address=str(sheet.cell(row=i, column=13).value), 	Telephone_Number=str(sheet.cell(row=i, column=14).value))
	#os.mkdir("word_docs")
	document4=MailMerge(template3)
	document4.merge(Entity_Name=str(sheet.cell(row=i, column=1).value),	bidding_document_price=str(sheet.cell(row=i, column=2).value),bidding_document_security=str(sheet.cell(row=i, column=3).value),	Procurement_Method=str(sheet.cell(row=i, column=4).value), Location=str(sheet.cell(row=i, column=5).value),	tender_subject=str(sheet.cell(row=i, column=6).value),	publish_date=str(sheet.cell(row=i, column=7).value),Prebid_Meeting_Date=str(sheet.cell(row=i, column=8).value),	Bid_Open_Date=str(sheet.cell(row=i, column=9).value),	Bid_Closing=str(sheet.cell(row=i, column=10).value), Bid_Winner_Declaration=str(sheet.cell(row=i, column=11).value),	Contract_Signing=str(sheet.cell(row=i, column=12).value), Address=str(sheet.cell(row=i, column=13).value), 	Telephone_Number=str(sheet.cell(row=i, column=14).value))
	#os.mkdir("word_docs")
	document5=MailMerge(template4)
	document5.merge(Entity_Name=str(sheet.cell(row=i, column=1).value),	bidding_document_price=str(sheet.cell(row=i, column=2).value),bidding_document_security=str(sheet.cell(row=i, column=3).value),	Procurement_Method=str(sheet.cell(row=i, column=4).value), Location=str(sheet.cell(row=i, column=5).value),	tender_subject=str(sheet.cell(row=i, column=6).value),	publish_date=str(sheet.cell(row=i, column=7).value),Prebid_Meeting_Date=str(sheet.cell(row=i, column=8).value),	Bid_Open_Date=str(sheet.cell(row=i, column=9).value),	Bid_Closing=str(sheet.cell(row=i, column=10).value), Bid_Winner_Declaration=str(sheet.cell(row=i, column=11).value),	Contract_Signing=str(sheet.cell(row=i, column=12).value), Address=str(sheet.cell(row=i, column=13).value), 	Telephone_Number=str(sheet.cell(row=i, column=14).value))
	#os.mkdir("word_docs")
	#os.chdir("word_docs") 
	document1.write(str(sheet.cell(row=i, column=15).value)+".docx")
	document2.write(str(sheet.cell(row=i, column=16).value)+".docx")
	document3.write(str(sheet.cell(row=i, column=17).value)+".docx")
	document4.write(str(sheet.cell(row=i, column=18).value)+".docx")
	document5.write(str(sheet.cell(row=i, column=19).value)+".docx")



#Code for the conversion from docx to pdf 
wdFormatPDF = 17

base_path="C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/"
os.chdir(base_path)
with os.scandir(base_path) as entries:
	for entry in entries:
		if entry.is_file():
			in_file=base_path+entry.name
			out_file=base_path+entry.name[:-4]+"pdf"
			word = comtypes.client.CreateObject('Word.Application')
			doc = word.Documents.Open(in_file)
			doc.SaveAs(out_file, FileFormat=wdFormatPDF)
			doc.Close()
			word.Quit()  


#Code for the conversion from pdf to png 

basepath="C:/Users/amroy/Desktop/transparency/lab_in_the_field/asiku_work/new/"

#os.chdir("basepath")
with os.scandir(basepath) as entries:
	for entry in entries:
		if entry.is_file():
			if entry.name[-3:]=="pdf":
				input_filename=basepath+"/"+entry.name
				images=convert_from_path(input_filename)
				for i, image in enumerate(images):
					fname=entry.name[:-4]+".PNG"
					image.save(fname,"PNG")


'''
			

	