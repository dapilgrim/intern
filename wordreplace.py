#Replace 
# read a text file, replace multiple words specified in a dictionary
# write the modified text back to a file
 
import re
 
def replace_words(text, word_dic):
    """
    take a text and replace words that match a key in a dictionary with
    the associated value, return the changed text
    """
    rc = re.compile('|'.join(map(re.escape, word_dic)))
    def translate(match):
        return word_dic[match.group(0)]
    return rc.sub(translate, text)
 
str1 = \
"""

"""
 
test_file = "Mword1.txt"
# create a test file for this example
fout = open(test_file, "w")
fout.write(str1)
fout.close()
 
# read the file
fin = open(test_file, "r")
str2 = fin.read()
fin.close()
 
# the dictionary has target_word:replacement_word pairs
word_dic = {
  'contact.AGRI': 'contact.AGRI_Y',
  'contact.AUTO':  'contact.AUTO_Y',
  'contact.BUILD': 'contact.BUILD_Y',
  'contact.CATER'  :'contact.CATER_Y' ,  
  'contact.CLEAN' : 'contact.CLEAN_Y' ,  
  'contact.CONS'  :'contact.CONS_Y',  
  'contact.FIN' : 'contact.FIN_Y' ,  
  'contact.REV'  : 'contact.REV_Y',  
  'contact.FOOD'  :'contact.FOOD_Y' ,  
  'contact.FUEL'  :'contact.FUEL_Y' ,  
  'contact.FURN'  :'contact.FURN_Y' ,  
  'contact.INDUS'  :'contact.INDUS_Y' ,  
  'contact.IT' : 'contact.IT_Y' ,  
  'contact.PR'  :'contact.PR_Y' ,  
  'contact.MED' :'contact.MED_Y' ,  
  'contact.OFF'  :'contact.OFF_Y' ,  
  'contact.ELEC'  :'contact.ELEC_Y' ,  
  'contact.PRINT'  :'contact.PRINT_Y' ,  
  'contact.READ'  :'contact.READ_Y' ,  
  'contact.SAFE'  :'contact.SAFE_Y' ,  
  'contact.TEX'  :'contact.TEX_Y' ,  
  'contact.TRAVEL' :'contact.TRAVEL_Y' ,  
  'contact.TRANSP'  :'contact.TRANSP_Y' ,  
  'contact.WASTE'  :'contact.WASTE_Y' , 
  'contact.FIRMNAME': 'contact.FIRMNAME_Y', 
  'contact.WATER'  : 'contact.WATER_Y',
  'contact.REIMBURSEX' : 'contact.REIMBURSEY', 
  'contact.TREATMENT': 'contact.TREATMENT_Y'
}
 
# call the function and get the changed text
str3 = replace_words(str2, word_dic)
 
# test
print (str3)
 
# write changed text back out
fout = open("Mword2.txt", "w")
fout.write(str3)
fout.close() 
