#Replace 
# read a text file, replace multiple words specified in a dictionary
# write the modified text back to a file
 
import re
 
def replace_words(text, word_dic):
    """
    take a text and replace words that match a key in a dictionary with
    the associated value, return the changed text
    """
    rc = re.compile('|'.join(map(re.escape, word_dic)))
    def translate(match):
        return word_dic[match.group(0)]
    return rc.sub(translate, text)
 
 
str1 = \
"""

"""
 
test_file = "Mword1.txt"
# create a test file for this example
fout = open(test_file, "w")
fout.write(str1)
fout.close()
 
# read the file
fin = open(test_file, "r")
str2 = fin.read()
fin.close()
 
# the dictionary has target_word:replacement_word pairs
word_dic = {
  'contact.AGRI': 'contact.AGRI_Y',
  'contact.AUTO':  'contact.AUTO_Y',
  'contact.BUILD': 'contact.BUILD_Y',
  'contact.CATER'  :'contact.CATER_Y' ,  
  'contact.CLEAN' : 'contact.CLEAN_Y' ,  
  'contact.CONS'  :'contact.CONS_Y',  
  'contact.FIN' : 'contact.FIN_Y' ,  
  'contact.REV'  : 'contact.REV_Y',  
  'contact.FOOD'  :'contact.FOOD_Y' ,  
  'contact.FUEL'  :'contact.FUEL_Y' ,  
  'contact.FURN'  :'contact.FURN_Y' ,  
  'contact.INDUS'  :'contact.INDUS_Y' ,  
  'contact.IT' : 'contact.IT_Y' ,  
  'contact.PR'  :'contact.PR_Y' ,  
  'contact.MED' :'contact.MED_Y' ,  
  'contact.OFF'  :'contact.OFF_Y' ,  
  'contact.ELEC'  :'contact.ELEC_Y' ,  
  'contact.PRINT'  :'contact.PRINT_Y' ,  
  'contact.READ'  :'contact.READ_Y' ,  
  'contact.SAFE'  :'contact.SAFE_Y' ,  
  'contact.TEX'  :'contact.TEX_Y' ,  
  'contact.TRAVEL' :'contact.TRAVEL_Y' ,  
  'contact.TRANSP'  :'contact.TRANSP_Y' ,  
  'contact.WASTE'  :'contact.WASTE_Y' , 
  'contact.FIRMNAME': 'contact.FIRMNAME_Y', 
  'contact.WATER'  : 'contact.WATER_Y',
  'contact.REIMBURSEX' : 'contact.REIMBURSEY', 
  'contact.TREATMENT': 'contact.TREATMENT_Y',
  'PDE1':'PDE1_Y',
'PDE2':'PDE2_Y',
'PDE3':'PDE3_Y',
'PDE4':'PDE4_Y',
'PDE5':'PDE5_Y',
'PDE6':'PDE6_Y',
'PDE7':'PDE7_Y',
'PDE8':'PDE8_Y',
'PDE9':'PDE9_Y',
'PDE10':'PDE10_Y',
'PDE11':'PDE11_Y',
'PDE12':'PDE12_Y',
'PDE13':'PDE13_Y',
'PDE14':'PDE14_Y',
'PDE15':'PDE15_Y',
'PDE16':'PDE16_Y',
'PDE17':'PDE17_Y',
'PDE18':'PDE18_Y',
'PDE19':'PDE19_Y',
'PDE20':'PDE20_Y',
'PDE21':'PDE21_Y',
'PDE22':'PDE22_Y',
'PDE23':'PDE23_Y',
'PDE24':'PDE24_Y',
'PDE25':'PDE25_Y',
'PDE26':'PDE26_Y',
'PDE27':'PDE27_Y',
'PDE28':'PDE28_Y',
'PDE29':'PDE29_Y',
'PDE30':'PDE30_Y',
'PDE31':'PDE31_Y',
'PDE32':'PDE32_Y',
'PDE33':'PDE33_Y',
'PDE34':'PDE34_Y',
'PDE35':'PDE35_Y',
'PDE36':'PDE36_Y',
'PDE37':'PDE37_Y',
'PDE38':'PDE38_Y',
'PDE39':'PDE39_Y',
'PDE40':'PDE40_Y',
'PDE41':'PDE41_Y',
'PDE42':'PDE42_Y',
'PDE43':'PDE43_Y',
'PDE44':'PDE44_Y',
'PDE45':'PDE45_Y',
'PDE46':'PDE46_Y',
'PDE47':'PDE47_Y',
'PDE48':'PDE48_Y',
'PDE49':'PDE49_Y',
'PDE50':'PDE50_Y',
'PDE51':'PDE51_Y',
'PDE52':'PDE52_Y',
'PDE53':'PDE53_Y',
'PDE54':'PDE54_Y',
'PDE55':'PDE55_Y',
'PDE56':'PDE56_Y',
'PDE57':'PDE57_Y',
'PDE58':'PDE58_Y',
'PDE59':'PDE59_Y',
'PDE60':'PDE60_Y',
'PDE61':'PDE61_Y',
'PDE62':'PDE62_Y',
'PDE63':'PDE63_Y',
'PDE64':'PDE64_Y',
'PDE65':'PDE65_Y',
'PDE66':'PDE66_Y',
'PDE67':'PDE67_Y'
}
 
# call the function and get the changed text
str3 = replace_words(str2, word_dic)
 
# test
print (str3)
 
# write changed text back out
fout = open("Mword4.txt", "w")
fout.write(str3)
fout.close() 
