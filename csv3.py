# -*- coding: utf-8 -*-
import os
import glob 
import  pandas as pd
# List all files in a directory using scandir()
basepath ='C:/Users/amroy/Dropbox/Newsletter/user_activity/Output/email_bounces/campaign_reports/253261'
os.chdir(basepath)
with os.scandir(basepath) as entries:
    for entry in entries:
        if entry.is_file():
            outfile=basepath+"/"+"concat.csv"
            fileList=glob.glob("*.csv")  
            dfList=[]
            for filename in fileList: 
                df=pd.read_csv(filename, sep=";", header=None)
                dfList.append(df)  
            concatDf=pd.concat(dfList, axis=0)
            concatDf.to_csv(outfile, index=None) 